import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");

import Review from "../../../../api/reviews/movieReviewModel";
import api from "../../../../index";
import reviews from "../../../../seedData/reviews";

const expect = chai.expect;
let db;

describe("movie reviews endpoint", () => {
   before(() => {
      mongoose.connect(process.env.MONGO_DB, {
         useNewUrlParser: true,
         useUnifiedTopology: true,
       });
       db = mongoose.connection; 
   })

   beforeEach(async () => {
      try {
        await Review.deleteMany();
        await Review.collection.insertMany(reviews);
      } catch (err) {
        console.error(`failed to Load user Data: ${err}`);
      }
    })

   afterEach(() => {
      api.close();
    });

   describe("GET /api/reviews", () => {
      it("should show all reviews", (done) => {
         request(api)
         .get("/api/reviews")
         .set("Accept", "application/json")
         .expect("Content-Type", /json/)
         .expect(200)
         .expect((res) => {
            expect(res.body).to.be.a("array");
            expect(res.body.length).to.equal(2);
            let result = res.body.map((review) => review.author);
            expect(result).to.have.members(["user1", "user2"]);
          })
          .end(done);
      })
   })
   describe("POST /api/reviews(?action=submit)", () => {
      it("missing movieID", () => {
         return request(api)
         .post('/api/reviews?action=submit')
         .send({
            'reviewId': 3,
            'author': 'user1',
            'content': 'movie review!',
            'rating': 'Good',
         })
         .expect(400)
         .expect({ success: false, msg: 'MovieID and ReviewID is required.' })
      })

      it("missing reviewID", () => {
         return request(api)
         .post('/api/reviews?action=submit')
         .send({
            'movieId': 238,
            'author': 'user1',
            'content': 'movie review!',
            'rating': 'Good',
         })
         .expect(400)
         .expect({ success: false, msg: 'MovieID and ReviewID is required.' })
      })

      it.skip("submitting same reviewID fail", () => {
         return request(api)
         .post('/api/reviews?action=submit')
         .send({
            'reviewId': 1,
            'movieId': 238,
            'author': 'user1',
            'content': '"movie review!"',
            'rating': 'Good',
         })
         .expect(500)
         .expect({ success: false, msg: 'Internal server error.' })
      })

      it("successful submission", () => {
         return request(api)
         .post('/api/reviews?action=submit')
         .send({
            'reviewId': 10,
            'movieId': 809,
            'author': 'user1',
            'content': '"movie review!"',
            'rating': 'Good',
         })
         .expect(201)
         .expect({ "success": true, "msg": "MovieReview successfully created." })
      })
   })
   describe("GET reviews by movie ID", () => {
      //movie with reviews
      it("get reviews by movieId: movie with reviews", () => {
         return request(api)
         .get('/api/reviews/238')
         .expect((res) => {
               expect(res.body).to.be.a("array");
               expect(res.body.length).to.equal(2);
         })
      })
      //movie with zero reviews
      it("get reviews by movieId: movie with 0 reviews", () => {
         return request(api)
         .get('/api/reviews/8')
         .expect((res) => {
            expect(res.body).to.be.a("array");
            expect(res.body.length).to.equal(0);
         })
      })

   describe("DELETE reviews by review ID", () => {
      it("non-existant review", () => {
         return request(api)
         .delete('/api/reviews/100')
         .expect(404)
         .expect({ "success": false, "msg": "Review not found." })
      })

      it("deleting existing review", () => {
         return request(api)
         .delete('/api/reviews/1')
         .expect(200)
         .expect({ "success": true, "msg": "MovieReview successfully deleted." })
      })
   })
})
})
