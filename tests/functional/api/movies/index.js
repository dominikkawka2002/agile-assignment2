import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");

import Movie from "../../../../api/movies/movieModel";
import api from "../../../../index";
import movies from "../../../../seedData/movies";

const expect = chai.expect;
let db;
let token = "";
let PersonId = 3
let page = Math.round(Math.random() * 10) //picks a random page from 1 to 10
let id = 238 //The Godfather
let invalidId = "invalid-id"

describe("Movies endpoint", () => {
  before(() => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = mongoose.connection;
  });

  beforeEach(async () => {
    try {
      await Movie.deleteMany();
      await Movie.collection.insertMany(movies);
    } catch (err) {
      console.error(`failed to Load user Data: ${err}`);
    }
    return request(api)
    .post("/api/users?action=authenticate")
    .send({
      username:"user1",
      password:"test123@"
    })
    .expect(200)
    .then((res) => {
      expect(res.body.success).to.be.true
      expect(res.body.token).to.not.be.undefined
      token = res.body.token.substring(7)
      //console.log(token)
    })
  })

  afterEach(() => {
    api.close(); // Release PORT 8080
  });

  describe("GET /api/movies ", () => {
    it("should return 10 movies and a status 200", (done) => {
      request(api)
        .get("/api/movies")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          expect(res.body.results).to.be.a("array");
          expect(res.body.results.length).to.equal(10);
          done();
        });
    });
  });

  describe("GET /api/movies/:id", () => {
    describe("when the id is valid", () => {
      it("should return the matching movie", () => {
        return request(api)
          .get(`/api/movies/${movies[0].id}`)
          .set("Accept", "application/json")
          .set("Authorization", token)
          .expect("Content-Type", /json/)
          .expect(200)
          .then((res) => {
            expect(res.body).to.have.property("title", movies[0].title);
          });
      });
    });
    describe("when the id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get("/api/movies/9999")
          .set("Accept", "application/json")
          .set("Authorization", token)
          .expect("Content-Type", /json/)
          .expect(404)
          .expect({
            status_code: 404,
            message: "The movie you requested could not be found.",
          });
      });
    });
  });

  describe("TMDB movie api endpoints", () => {
    describe("general endpoints", () => {
      it("GET /tmdb/discover/:page", () => {
        return request(api)
          .get(`/api/movies/tmdb/discover/${page}`)
          .set("Accept", "application/json")
          .set("Authorization", token)
          .expect("Content-Type", /json/)
          .expect(200)
      })
      it("GET /tmdb/movie/:id (existing movie)", () => {
        return request(api)
          .get(`/api/movies/tmdb/movie/${id}`)
          .set("Accept", "application/json")
          .set("Authorization", token)
          .expect("Content-Type", /json/)
          .expect(200)
          .then((res) => {
            expect(res.body).to.have.property("title", "The Godfather");
          });
      })
      it("GET /tmdb/movie/:id (non-existing movie)", () => {
        return request(api)
          .get(`/api/movies/tmdb/movie/${invalidId}`)
          .set("Accept", "application/json")
          .set("Authorization", token)
          .expect(500) //Internal server error as movie with id doesn't exist
          .then((res) => {
            expect(res.text).to.contain("Hey!! You caught the error 👍👍.")
          })
      })
      it("GET /tmdb/genres/", () => {
        return request(api)
          .get(`/api/movies/tmdb/genres`)
          .set("Accept", "application/json")
          .set("Authorization", token)
          .expect("Content-Type", /json/)
          .expect(200)
          .then((res) => {
            expect(res.body.genres).to.be.a("array");
          });
      })
      it("GET /tmdb/movieImages/:id (existing movie)", () => {
        return request(api)
          .get(`/api/movies/tmdb/movieImages/${id}`)
          .set("Accept", "application/json")
          .set("Authorization", token)
          .expect("Content-Type", /json/)
          .expect(200)
          .then((res) => {
            expect(res.body.backdrops).to.be.a("array");
          });
      })
      it("GET /tmdb/movieImages/:id (non-existing movie)", () => {
        return request(api)
          .get(`/api/movies/tmdb/movieImages/${invalidId}`)
          .set("Accept", "application/json")
          .set("Authorization", token)
          .expect(500) //Internal server error as movie with id doesn't exist
          .then((res) => {
            expect(res.text).to.contain("Hey!! You caught the error 👍👍.")
          })
      })
      it("GET /tmdb/movie/:id/reviews (existing movie)", () => {
        return request(api)
          .get(`/api/movies/tmdb/movie/${id}/reviews`)
          .set("Accept", "application/json")
          .set("Authorization", token)
          .expect("Content-Type", /json/)
          .expect(200)
          .then((res) => {
            expect(res.body).to.be.a("array");
          });
      })
      it("GET /tmdb/movie/:id/reviews (non-existing movie)", () => {
        return request(api)
          .get(`/api/movies/tmdb/movie/${invalidId}/reviews`)
          .set("Accept", "application/json")
          .set("Authorization", token)
          .expect("Content-Type", /json/)
          .expect(200)
          .then((res) => {
            expect(res.body).to.be.empty
          });
      })
      it("GET /tmdb/upcoming/:page", () => {
        return request(api) 
          .get(`/api/movies/tmdb/upcoming/${page}`)
          .set("Accept", "application/json")
          .set("Authorization", token)
          .expect("Content-Type", /json/)
          .expect(200)
      })
      it("GET /tmdb/popular/:page", () => {
        return request(api)
          .get(`/api/movies/tmdb/popular/${page}`)
          .set("Accept", "application/json")
          .set("Authorization", token)
          .expect("Content-Type", /json/)
          .expect(200)
      })
      it("GET /tmdb/topRated/:page", () => {
        return request(api)
          .get(`/api/movies/tmdb/topRated/${page}`)
          .set("Accept", "application/json")
          .set("Authorization", token)
          .expect("Content-Type", /json/)
          .expect(200)
      })
      it("GET /tmdb/nowPlaying/:page", () => {
        return request(api)
          .get(`/api/movies/tmdb/nowPlaying/${page}`)
          .set("Accept", "application/json")
          .set("Authorization", token)
          .expect("Content-Type", /json/)
          .expect(200)
      })
    })
    describe("/movie/:id/ tests", () => {
      it("GET /tmdb/movie/:id/recommendations (existing movie)", () => {
         //console.log(token)
         return request(api)
            .get(`/api/movies/tmdb/movie/${id}/recommendations`)
            .set("Authentication", token)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((res) => {
               //console.log(res.body)
               expect(res.body.results).to.be.a("array")
            });
         })
      })
      it("GET /tmdb/movie/:id/similar (existing movie)", () => {
        //console.log(token)
        return request(api)
          .get(`/api/movies/tmdb/movie/${id}/similar`)
          .set("Authentication", token)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .then((res) => {
            //console.log(res.body)
            expect(res.body.results).to.be.a("array")
        });
      })
      it.skip("GET /tmdb/movie/:id/credits", () => {
        //console.log(token)
        return request(api)
          .get(`/api/movies/tmdb/movie/${id}/credits`)
          .set("Authentication", token)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .then((res) => {
            //console.log(res.body)
            expect(res.body.results.cast).to.be.a("array")
        });
      })
    describe("Person tests", () => {
      it("GET person/:id", () => {
        //console.log(token)
        return request(api)
          .get(`/api/movies/tmdb/person/${PersonId}`)
          .set("Authentication", token)
          //.set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .then((res) => {
            //console.log(res.body)
        });
      })
      it("GET person/:id/credits", () => {
        //console.log(token)
        return request(api)
          .get(`/api/movies/tmdb/person/${PersonId}/credits`)
          .set("Authentication", token)
          .set("Accept", "application/json")
          //.expect("Content-Type", /json/)
          .expect(200)
          .then((res) => {
            //console.log(res.body)
            //expect(res.body.results).to.be.a("array")
        });
      })
    })
  })
});
