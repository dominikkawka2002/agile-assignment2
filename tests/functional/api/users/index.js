import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import User from "../../../../api/users/userModel";
import api from "../../../../index";

const expect = chai.expect;
let db;
let user1token;
let user1id;

describe("Users endpoint", () => {
  before(() => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = mongoose.connection;
  });

  beforeEach(async () => {
    try {
      await User.deleteMany();
      // Register two users
      await request(api).post("/api/users?action=register").send({
        username: "user1",
        password: "test123@",
      });
      await request(api).post("/api/users?action=register").send({
        username: "user2",
        password: "test123@",
      });
    } catch (err) {
      console.error(`failed to Load user test Data: ${err}`);
    }
  });
  afterEach(() => {
    api.close();
  });
  describe("GET /api/users ", () => {
    it("should return the 2 users and a status 200", (done) => {
      request(api)
        .get("/api/users")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          expect(res.body).to.be.a("array");
          expect(res.body.length).to.equal(2);
          let result = res.body.map((user) => user.username);
          expect(result).to.have.members(["user1", "user2"]);
          done();
        });
    });
  });

  describe("POST /api/users ", () => {
    describe("For a register action", () => {
      describe("when the payload is correct", () => {
        it("should return a 201 status and the confirmation message", () => {
          return request(api)
            .post("/api/users?action=register")
            .send({
              username: "user3",
              password: "test123@",
            })
            .expect(201)
            .expect({ success: true, msg: 'User successfully created.' });
        });
        after(() => {
          return request(api)
            .get("/api/users")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((res) => {
              expect(res.body.length).to.equal(3);
              const result = res.body.map((user) => user.username);
              expect(result).to.have.members(["user1", "user2", "user3"]);
              user1id = res.body[0]._id
              //console.log("register: "+user1id)
            });
        });
      });
    });
    describe("For an authenticate action", () => {
      describe("when the payload is correct", () => {
        it("should return a 200 status and a generated token", () => {
          return request(api)
            .post("/api/users?action=authenticate")
            .send({
              username: "user1",
              password: "test123@",
            })
            .expect(200)
            .then((res) => {
              expect(res.body.success).to.be.true;
              expect(res.body.token).to.not.be.undefined;
              user1token = res.body.token.substring(7);
            });
        });
      });
    });
    describe("failed registration", () => {
      //missing password
      it("missing password", () => {
        return request(api)
        .post("/api/users?action=register")
        .send({
          username: "user10",
        })
        .expect(400)
        .expect({ success: false, msg: 'Username and password are required.' });
      })
      it("missing username", () => {
        return request(api)
        .post("/api/users?action=register")
        .send({
          password: "pass10",
        })
        .expect(400)
        .expect({ success: false, msg: 'Username and password are required.' });
      })
      //password too short
      //
      // TODO: SILENT PRINCIPLE. 
      //
      it.skip("password too short", async () => {
        try {
        return request(api)
        .post("/api/users?action=register")
        .send({
          username: "user20",
          password: "short"
        })
        .expect(500)
        .expect({ success: false, msg: 'Internal server error.' });
      } catch (error) {
        expect(error.code).to.equal(11000)
        expect(error.name).to.equal('MongoServerError');
      }

      //username already exists
      //
      // TODO: SILENT PRINCIPLE. 
      //
      it("username already exists", () => {
        return request(api)
        .post("/api/users?action=register")
        .send({
          username: "user1",
          password: "test456@"
        })
        .expect(500)
        .expect({ success: false, msg: 'Internal server error.' });
      })
    })

    describe("failed logins", () => {
      //missing username
      it("missing username", () => {
        return request(api)
        .post("/api/users")
        .send({
          password: "test123@"
        })
        .expect(400)
        .expect({ success: false, msg: 'Username and password are required.' });
      })
      //missing password
      it("missing password", () => {
        return request(api)
        .post("/api/users")
        .send({
          username: "user1"
        })
        .expect(400)
        .expect({ success: false, msg: 'Username and password are required.' });
      })
      //user doesn't exist
      it("user doesn't exist", () => {
        return request(api)
        .post("/api/users")
        .send({
          username: "user100",
          password: "test123@"
        })
        .expect(401)
        .expect({ success: false, msg: 'Authentication failed. User not found.' });
      })
      //incorrect password for user
      it("incorrect password for existing user", () => {
        return request(api)
        .post("/api/users")
        .send({
          username: "user1",
          password: "test12"
        })
        .expect(401)
        .expect({ success: false, msg: 'Wrong password.' });
      })
    })
    describe.skip("update a user", () => {
      it("update user by id", () => {
        request(api)
          //console.log("patch: "+user1id)
          //TypeError: Cannot read properties of undefined (reading 'put')
          .put(`/api/users/${user1id}`)
          .send({
            "username": "user10",
            "password": "newTest123@"
          })
          .expect(200)
          .expect({ msg: 'User Updated Successfully' })
          //result = res.body.map((user) => user.username)
          //.expect(result).to.have.members(["user10"])
          //.expect(result).to.not.have.members(["user1"])
      }) 
    })
    describe("Set favourite movies for user", () => {
      it("set movie favourite by username", () => {
        request(api)
          .patch("/api/users/user1")
          .send({
            favourites: [{
              movieId: 238,
              movieId: 501
            }]
          })
        .expect(200)
        .expect({ msg: 'User Updated Successfully' })
      })
      after(() => {
        request(api)
          .get("/api/users")
          .then((res) => {
            //console.log(res.body)
          })
        })
      })
    })
  });
});
