# Assignment 2 - Agile Software Practice.

Name: Dominik Kawka

## API endpoints.

[List the Web API's endpoints and state the purpose of each one. Indicate those that require authentication.]
 
 e.g.

 + `/tmdb/movie/:id`: Get movie by id
 + `/tmdb/movieImages/:id`: Get movie images
 + `/tmdb/movieReviews/:id`: Get movie reviews
 + `/tmdb/movie/:id/recommendations`: Get movie recommendations
 + `/tmdb/movie/:id/similar`: Get similar movie 
 + `/tmdb/discover/:page`: Gets discover page
 + `/tmdb/popular/:page`: Gets popular releases
 + `/tmdb/topRated/:page`: Gets top rated releases
 + `/tmdb/upcoming/:page`: Gets upcoming releases
 + `/tmdb/nowPlaying/:page`: Gets movies currently playing
 + `/tmdb/movie/:id/credits`: Get movie credits
 + `/tmdb/person/:id`: Get person id
 + `/tmdb/person/:id/credits`: Get person credits
 + `/tmdb/movie/:id/reviews`: get movie reviews from TMDB
 + `reviews`: get/post reviews from/to database
 + `reviews/:reviewId`: Delete certain review
 + `reviews/:movieId`: get reviews for certain movie

## Automated Testing.

A few tests are skipped to maintain silent principle, as a MongoServerError will print in the terminal otherwise. 

~~~
    Users endpoint
    GET /api/users
database connected to movies on ac-bgw1dvk-shard-00-01.ov2ufxg.mongodb.net
      √ should return the 2 users and a status 200
    POST /api/users 
      For a register action
        when the payload is correct
          √ should return a 201 status and the confirmation message (93ms)
      For an authenticate action
        when the payload is correct
          √ should return a 200 status and a generated token (89ms)
      failed registration
        √ missing password
        √ missing username
        - password too short
        failed logins
          √ missing username
          √ missing password
          √ user doesn't exist
          √ incorrect password for existing user (86ms)
        update a user
          - update user by id
        Set favourite movies for user
          √ set movie favourite by username

  Movies endpoint
    GET /api/movies
      √ should return 10 movies and a status 200
    GET /api/movies/:id
      when the id is valid
        √ should return the matching movie
      when the id is invalid
        √ should return the NOT found message
    TMDB movie api endpoints
      √ GET /tmdb/movie/:id/similar (existing movie) (87ms)
      - GET /tmdb/movie/:id/credits
      general endpoints
        √ GET /tmdb/discover/:page (136ms)
        √ GET /tmdb/movie/:id (existing movie) (384ms)
        √ GET /tmdb/movie/:id (non-existing movie) (147ms)
        √ GET /tmdb/genres/ (68ms)
        √ GET /tmdb/movieImages/:id (existing movie) (58ms)
        √ GET /tmdb/movieImages/:id (non-existing movie) (142ms)
        √ GET /tmdb/movie/:id/reviews (existing movie) (63ms)
        √ GET /tmdb/movie/:id/reviews (non-existing movie) (211ms)
        √ GET /tmdb/upcoming/:page (65ms)
        √ GET /tmdb/popular/:page (61ms)
        √ GET /tmdb/topRated/:page (61ms)
        √ GET /tmdb/nowPlaying/:page (73ms)
      /movie/:id/ tests
        √ GET /tmdb/movie/:id/recommendations (existing movie) (60ms)
      Person tests
        √ GET person/:id (62ms)
        √ GET person/:id/credits (79ms)

  movie reviews endpoint
    GET /api/reviews
      √ should show all reviews
    POST /api/reviews(?action=submit)
      √ missing movieID
      √ missing reviewID
      - submitting same reviewID fail
      √ successful submission
    GET reviews by movie ID
      √ get reviews by movieId: movie with reviews
      √ get reviews by movieId: movie with 0 reviews
      DELETE reviews by review ID
        √ non-existant review
        √ deleting existing review (62ms)


  37 passing (9s)
  4 pending

--------------------------------|---------|----------|---------|---------|-----------------------------------------
File                            | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s
--------------------------------|---------|----------|---------|---------|-----------------------------------------
All files                       |   82.24 |    58.33 |   86.97 |   83.68 | 
 movies-api-cicd                |     100 |      100 |     100 |     100 | 
  index.js                      |     100 |      100 |     100 |     100 | 
 movies-api-cicd/api            |   86.02 |    61.61 |   87.93 |   85.05 | 
  tmdb-api.js                   |   86.02 |    61.61 |   87.93 |   85.05 | 140,145,154,159,165-174,183,188,197,202
 movies-api-cicd/api/movies     |   96.44 |    78.72 |   95.58 |   97.64 | 
  index.js                      |   96.29 |    78.26 |   95.45 |   97.46 | 107-108
  movieModel.js                 |     100 |      100 |     100 |     100 | 
 movies-api-cicd/api/reviews    |   96.29 |    76.66 |     100 |   97.72 | 
  index.js                      |   95.71 |    75.86 |     100 |   97.05 | 23
  movieReviewModel.js           |     100 |      100 |     100 |     100 | 
 movies-api-cicd/api/users      |   81.19 |    59.04 |   84.21 |   82.85 | 
  index.js                      |    75.9 |    61.76 |   76.92 |   74.46 | 28,35-43,48-55
  userModel.js                  |   94.11 |    54.05 |     100 |     100 | 2-26
 movies-api-cicd/authenticate   |   21.87 |    21.62 |      50 |      25 | 
  index.js                      |   21.87 |    21.62 |      50 |      25 | 6-23
 movies-api-cicd/db             |   83.33 |      100 |      50 |   81.81 | 
  index.js                      |   
  83.33 |      100 |      50 |   81.81 | 11,14
 movies-api-cicd/errHandler     |      80 |       50 |     100 |      80 | 
  index.js                      |      80 |       50 |     100 |      80 | 5
 movies-api-cicd/initialise-dev |     100 |      100 |     100 |     100 | 
  movies.js                     |     100 |      100 |     100 |     100 | 
  users.js                      |     100 |      100 |     100 |     100 | 
 movies-api-cicd/seedData       |   26.66 |     6.97 |    12.5 |   37.93 | 
  index.js                      |   19.51 |     6.97 |    12.5 |      28 | 10-34,37-38
  movies.js                     |     100 |      100 |     100 |     100 | 
  reviews.js                    |     100 |      100 |     100 |     100 | 
--------------------------------|---------|----------|---------|---------|-----------------------------------------


  6 passing (6s)
~~~

## Deployments.
- STAGING: `https://agile-assignment2-dk-7ed6e1a7b96f.herokuapp.com/`
- PROD: `https://agile-assignment2-prod-80d3a9a27a70.herokuapp.com/`

## Independent Learning (if relevant)
1. `Istanbul + coveralls`
I followed the coveralls start uploading coverage guide, along with the node guide on (github)[https://github.com/nickmerwin/node-coveralls?tab=readme-ov-file].

The screenshot below includes an image of the coverall reports, including the tests that ran from the pipeline. *marked as branch '?'*
![Coverall Report](/readmeImages/coverAllsReport.PNG)


