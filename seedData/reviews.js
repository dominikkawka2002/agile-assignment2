const reviews = [
   {
      'movieId': 238,
      'reviewId': 1,
      'author': 'user1',
      'content': 'The Godfather is an excellent movie',
      'rating': 'Excellent',
   },
   {
      'movieId': 238,
      'reviewId': 2,
      'author': 'user2',
      'content': 'The Godfather is in my top 3 favourite movies!',
      'rating': 'Excellent',
   },
];
export default reviews;